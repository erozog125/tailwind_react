import Home from './Components/Pages/Home/index.jsx';
import './App.css';

function App() {
  return (
    <div>
      <Home />
    </div>
  );
}

export default App;
