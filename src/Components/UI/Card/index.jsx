export default function Card({ img, alt, description }) {
  return(
    <>
      <div className="my-1 px-1 ">
        <article className="border-2 rounded-lg">
          <img className='block w-full border-2 rounded-lg' alt={alt} src={img} />    
          <header className="flex items-center justify-between p-2 md:p-4">
            <p className="text-lg h-16">
              {description}
            </p>            
          </header>          
        </article>   
      </div>      
    </>
  );
}