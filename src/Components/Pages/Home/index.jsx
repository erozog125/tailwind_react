import Card from '../../UI/Card/index.jsx';
import img1 from '../../../assets/images/cv/img1.jpg';
import img2 from '../../../assets/images/cv/img2.jpg';
import img3 from '../../../assets/images/cv/img3.jpg';
import img4 from '../../../assets/images/cv/img4.jpg';


export default function Home() {
  return(
    <>
    <h1 className='text-blue-600 border-blue-900'>Practice - Tailwind - Edwin Rozo Gómez</h1>    <div className='grid grid-cols-1 gap-12 md:grid-cols-4'>
      <Card
          img = {img1}
          alt = 'Prueba1'
          description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        />      
      <Card
          img = {img2}
          alt = 'Prueba2'
          description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        />      
      <Card
          img = {img3}
          alt = 'Prueba3'
          description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        />      
      <Card
          img = {img4}
          alt = 'Prueba4'
          description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."          
        />      
    </div>      
    </>
    );
  }